<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>VueJS & Laravel</title>

        <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    </head>
    <body class="font-sans text-black antialiased">
        <div id="app" class="h-screen w-full max-w-md w-full mx-auto">
            <h1 class="m-8 font-normal">{{ $title }}</h1>

            <comments class="mx-8"></comments>
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>
